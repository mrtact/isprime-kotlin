

fun isPrime(n: Long): Boolean {
    if (n == 0L) {
        return false
    }

    if (n == 1L || n == 2L) {
        return true
    }

    if (n % 2 == 0L) {
        return false
    }

    val limit = Math.sqrt(n.toDouble()).toLong()
    for (i in 3..limit step 2) {
        if (n % i == 0L) {
            return false
        }
    }

    return true
}
