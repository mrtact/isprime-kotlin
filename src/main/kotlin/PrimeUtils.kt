package main.kotlin.primeutils

import seq
import sqrt

/**
 * Assumes n is non-negative, because sqrt of negative is NaN
 */
val Long.isPrime: Boolean
    get() {
        require(this >= 0L)
        return when (this) {
            0L -> false
            in 1L..2L -> true
            else -> (2L seq (3..sqrt step 2)).none { this % it == 0L }
        }
    }
