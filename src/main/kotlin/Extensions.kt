import main.kotlin.primeutils.isPrime
import kotlin.math.sqrt

infix fun <T> T.seq(iter: Iterable<T>) = sequenceOf(this) + iter.asSequence()
infix fun <T> T.seq(elem: T) = sequenceOf(this, elem)
infix fun <T> Iterable<T>.seq(elem: T) = this.asSequence() + elem
infix fun <T> Iterable<T>.seq(iter: Iterable<T>) = this.asSequence() + iter.asSequence()
infix fun <T> Sequence<T>.seq(elem: T) = this + elem
infix fun <T> Sequence<T>.seq(iter: Iterable<T>) = this + iter.asSequence()


fun <T> uberSequenceOf(vararg args: Any) = args.map {
    when (it) {
        is Array<*> -> it.iterator().asSequence()
        is Iterable<*> -> it.asSequence()
        is Sequence<*> -> it
        else -> sequenceOf(it)
    } as Sequence<T>
}.asSequence().flatten()

val Int.isPrime
    get() = this.toLong().isPrime

/**
 * Note that this truncates, and we're okay with that.
 */
val Long.sqrt
    get() = sqrt(this.toDouble()).toLong()
