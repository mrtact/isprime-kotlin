public class JavaPrime {

    public static boolean isPrime(long num) {
        if (num == 0L) {
            return false;
        }

        if (num == 1L || num == 2L) {
            return true;
        }

        if (num % 2 == 0L) {
            return false;
        }

        long limit = (long) Math.sqrt(num);
        for (long i = 3; i <= limit; i += 2) {
            if (num % i == 0L) {
                return false;
            }
        }

        return true;
    }
}
