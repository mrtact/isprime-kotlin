package test.kotlin

import main.kotlin.primeutils.isPrime
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

/**
 * FYI: running dynamic JUnit 5 tests requires IntelliJ 2017.2.4 or better, or you have to explicitly import
 * a test runner that supports them. @see https://stackoverflow.com/questions/46161608/intellij-junit-5-jupiter
 */
public class TestMain {
    @TestFactory
    fun testPrimes() = listOf(1, 2, 3, 11, 71, 30_571_351, 70_368_760_954_879).map { testPrime(it) }

    @TestFactory
    fun testNonPrimes() = listOf(0L, 4, 9, 20, 49, 121).map { testPrime(it, false) }
}

fun testPrime(n: Long, shouldBePrime: Boolean = true): DynamicTest {
    val displayName = "$n should ${if (!shouldBePrime) "not " else ""}be prime"
    return dynamicTest(displayName) {
        assertEquals(shouldBePrime, n.isPrime)
    }
}
